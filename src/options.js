import cli from "../package.json";
import merge from "deepmerge";
import sort from "sort-package-json";
import getopts from "getopts";
import { safePackageName } from "./utils";
import { lastPathSegment } from "./commands/init";
import { createTsConfig } from "./commands/init-create";
import { write, bright, reset, red, yellow } from "./utils";
import { existsSync, unlinkSync, renameSync } from "fs";

let converted_pkg = "./package.pre_conversion.json";
let converted_tsconfig = "./tsconfig.pre_conversion.json";

export const getDefaultPackagePlugins = () => ({
    // https://rollupjs.org/guide/en/#error-name-is-not-exported-by-module
    // apparently namedExports is deprecated.
    // commonjs: {
    //     namedExports: {
    //         react: ["createElement", "Component"],
    //         mir: ["h"],
    //     },
    // },
    terser: {
        toplevel: true,
        keep_fnames: true,
        mangle: { properties: { regex: "^[\\$\\$]" } },
        compress: { passes: 10, pure_getters: true },
        // https://github.com/rollup/plugins/tree/master/packages/pluginutils#usage-1
        filter: {
            include: [],
            exclude: [],
            resolve: false,
        },
    },
    pragmatic: {
        module: "mir", // from the mir module
        export: "h", // using the named export "h"
        import: "h", // import it as "h" into every file that uses JSX
    },
    server: {
        entry: "public/index.html",
        port: "1234",
        openBrowser: false,
    },
});

export const getDefaultPackageOutputs = (name) => ({
    main: `dist/${name}.cjs`,
    module: `dist/${name}.mjs`,
    browser: `dist/${name}.umd.js`,
});

export const getDefaultPackageExports = (name, defaultExport) => ({
    // https://nodejs.org/api/packages.html#packages_conditional_exports
    // The package type determining whether to load .js files as CommonJS or ES modules
    node: {
        // "import" - matches when the package is loaded via import or import(),
        // or via any top-level import or resolve operation by the ECMAScript module loader.
        import: `./dist/${name}.mjs`,
        // "require" - matches when the package is loaded via require().
        // The referenced file should be loadable with require() although the condition
        // matches regardless of the module format of the target file. Expected
        // formats include CommonJS, JSON, and native addons but not ES modules as
        // require() doesn't support them. Always mutually exclusive with "import".
        require: `./dist/${name}.cjs`,
    },
    // When using environment branches, always include a "default" condition where possible.
    // Providing a "default" condition ensures that any unknown JS environments are able
    // to use this universal implementation, which helps avoid these JS environments from
    // having to pretend to be existing environments in order to support packages with
    // conditional exports. For this reason, using "node" and "default" condition branches
    // is usually preferable to using "node" and "browser" condition branches.
    default: `./dist/${name}.${defaultExport ?? "umd.js"}`,
});

export const changePackage = async (pkg) => {
    if (!existsSync("./package.json")) return false;
    let name = pkg.name || lastPathSegment(process.cwd());

    await write(converted_pkg, JSON.stringify(pkg, null, 4));

    pkg = merge(pkg, {
        ...getDefaultPackageOutputs(name),
        type: "module",
        exports: pkg.exports ?? { ...getDefaultPackageExports(name) },
        source: pkg.source ?? pkg.main ?? "src/index.js",
        files: pkg.files ?? ["dist"],
        scripts: pkg.scripts ?? { build: "roller build", start: "roller start" },
        roller: { globals: {}, external: [], plugins :{ ...getDefaultPackagePlugins() }},
    });

    await write(`./package.json`, JSON.stringify(sort(pkg), null, 4));
    return true;
};

export const changeTsconfig = async () => {
    if (existsSync("./tsconfig.json")) {
        renameSync("./tsconfig.json", converted_tsconfig);
        await write("./tsconfig.json", createTsConfig());
        return true;
    }
    return false;
};

export const revertTsconfig = async () => {
    if (existsSync(converted_tsconfig)) {
        unlinkSync("./tsconfig.json");
        renameSync(converted_tsconfig, "./tsconfig.json");
        return true;
    }
    return false;
};

export const revertPackage = async () => {
    if (existsSync(converted_pkg)) {
        unlinkSync("./package.json");
        renameSync(converted_pkg, "./package.json");
        return true;
    }
    return false;
};

export const getOptions = (pkg, command) => {
	const {
		roller = {},
		source = "src/index.js",
		browserslist = "last 2 Chrome versions or last 2 ChromeAndroid versions or last 2 Safari versions or last 2 iOS versions or last 2 Firefox versions or last 2 FirefoxAndroid versions or last 2 Edge versions or last 2 Opera versions or last 2 OperaMobile versions and >1%",
		main,
		module,
		browser,
		types,
	} = pkg;

	const {
		name = pkg.name || process.cwd(),
		sourcemap = command !== "start",
		minify = command !== "start",
		target = "es",
		globals = {},
		example = "public/index.js",
		runtime = "classic",
		pragma = "React.createElement",
		pragmaFrag = "React.Fragment",
        plugins = {},
		usets = false,
		debug = false,
	} = roller;

    const {
        entry = "public/index.html",
        port = "1234",
        openBrowser = false,
    } = plugins.server || {};

	const options = getopts(process.argv.slice(3), {
		boolean: ["debug", "sourcemap", "minify", "usets", "openBrowser"],
        alias: {
            debug: "d",
            source: "s",
            port: "p",
            target: "t",
        },
		string: ["cjs", "umd", "esm", "types", "runtime", "source"],
		default: {
			name: safePackageName(name),
			source,
			port,
			target,
			sourcemap,
			minify,
			entry,
			example,
			browserslist,
			usets,
			pragma,
			pragmaFrag,
			debug,
            openBrowser,
		},
	});

	if (!options.cjs && !options.esm && !options.umd && !options.types) {
		options.main = main;
		options.module = module;
		options.browser = browser;
		options.types = types;
	} else {
		if (options.cjs) options.main = options.cjs;
		if (options.esm) options.module = options.esm;
		if (options.umd) options.browser = options.umd;
	}

	return { ...options, globals, runtime };
};
