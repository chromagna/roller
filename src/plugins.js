import json from "@rollup/plugin-json";
import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import replace from "@rollup/plugin-replace";
import nodePolyfills from "rollup-plugin-polyfill-node";
import babel from "@rollup/plugin-babel";
import typescript from "@rollup/plugin-typescript";

import { terser } from "./plugins/terser";
import { sizer } from "./plugins/sizer";
import { server } from "./plugins/server";

import { babelConfig } from "./babel";

export const plugins = (command, pkg, options) => {
    const { extensions, presets, plugins } = babelConfig(command, pkg, options);
    const { sourcemap, minify, entry, port, usets, openBrowser } = options;
    const babelDefaults = { babelrc: false, configFile: false, compact: false };

    return [
        json(),
        commonjs({
            extensions,
            include: /node_modules/,
            // namedExports is deprecated
            // namedExports: pkg.roller?.plugins?.commonjs?.namedExports ?? {},
        }),
        babel({
            ...babelDefaults,
            exclude: "node_modules/**",
            extensions,
            presets,
            plugins,
            sourceMap: sourcemap,
            babelHelpers: "bundled",
            inputSourceMap: sourcemap,
        }),
        nodeResolve({
            mainFields: ["module", "jsnext:main", "browser", "main"],
            extensions,
        }),
        nodePolyfills({ sourceMap: sourcemap }),
        usets && typescript(),
        replace({ preventAssignment: true, values: { "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV) } }),
        command !== "start"
            && minify
            && terser(
                { sourceMap: sourcemap },
                {
                    include: pkg.roller?.plugins?.terser?.filter?.include ?? [],
                    exclude: pkg.roller?.plugins?.terser?.filter?.exclude ?? [],
                    resolve: pkg.roller?.plugins?.terser?.filter?.resolve ?? false,
                },
                pkg
            ),
        command === "build" && sizer(),
        command === "start" && server({ entry, port, openBrowser }),
    ].filter(Boolean);
};

export const dtsPlugins = () => {
    return [require("rollup-plugin-dts").default(), sizer()];
};
