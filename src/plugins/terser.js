import path from "path";
import { readFileSync, existsSync, writeFileSync } from "fs-extra";
import { minify } from "terser";
import { codeFrameColumns } from "@babel/code-frame";
import { createFilter } from "@rollup/pluginutils";
import merge from "deepmerge";

const transform = async (code, options) => {
	return await minify(code, options).catch((err) => {
		throw err;
	});
};

export const terser = (options = {}, terseroptions = {}, pkg = {}) => {
	const filter = createFilter(
        terseroptions.include,
        terseroptions.exclude,
        {
            resolve: terseroptions.resolve,
        });

	let hasRc;
	let options_;

	const rc = path.join(process.cwd(), ".terserrc");

	if (existsSync(rc)) {
		options_ = JSON.parse(readFileSync(rc, "utf-8"));
		hasRc = true;
	} else {
		options_ = {
			toplevel: pkg.roller?.plugins?.terser?.toplevel ?? true,
            // usefor for code relying on Function.prototype.name
            keep_fnames: pkg.roller?.plugins?.terser?.keep_fnames ?? true,
			// mangle: { properties: { regex: "^[_|\\$]" } },

            // only mangle object properties that begin with _
			// mangle: { properties: { regex: "^[_]" } },

            // only mange object properties that begin with $$
			// mangle: { properties: { regex: "^[\\$\\$]" } },
			mangle: pkg.roller?.plugins?.terser?.mangle ?? { properties: { regex: "^[\\$\\$]" } },
			compress: pkg.roller?.plugins?.terser?.compress ?? { passes: 10, pure_getters: true },
		};
		hasRc = false;
	}

	const finalOptions = merge(options_, options);

	return {
		name: "terser",

		async renderChunk(code, chunk) {
			if (!filter(chunk.fileName)) return null;

			let result;

			try {
				result = await transform(code, finalOptions);
                if (hasRc && finalOptions.nameCache) {
                    options_.nameCache = finalOptions.nameCache;
                    writeFileSync(rc, JSON.stringify(options_, null, "  ") + "\n", "utf-8");
                }
			} catch (err) {
				const { message, line, col: column } = err;
				console.error(codeFrameColumns(code, { start: { line, column } }, { message }));
				throw err;
			}

			return {
				code: result.code,
				map: result.map,
			};
		},
	};
};
