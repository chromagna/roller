import { serve } from "../server/server";

let ran = false;

export const server = (options) => {
	return {
		name: "server",
		generateBundle() {
			if (!ran) {
				serve({ ...options });
				ran = true;
			}
		},
	};
};
