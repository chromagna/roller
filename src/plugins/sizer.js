import gzip from "gzip-size";
import prettyBytes from "pretty-bytes";
import { bright, reset } from "../utils";

export const sizer = () => {

	const showSize = (bundle) => {
		const { code, fileName } = bundle;
		const pretty = prettyBytes(gzip.sync(code));
		console.log(` ${bright}Size: ${reset}${prettyBytes(code.length)} ${bright}Gzipped: ${reset}${pretty}${reset} -> ${bright}${fileName}${reset}`);
	};

	return {
		name: "sizer",
		generateBundle(_, bundle, isWrite) {
			if (isWrite) {
				Object.keys(bundle)
					.map((file) => bundle[file])
					.filter((bundle) => !bundle.isAsset)
					.forEach((bundle) => showSize(bundle));
			}
		},
	};
};
