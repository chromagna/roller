import { DEFAULT_EXTENSIONS } from "@babel/core";

import presetEnv from "@babel/preset-env";
import presetTs from "@babel/preset-typescript";
import presetReact from "@babel/preset-react";

import pluginDecorators from "@babel/plugin-proposal-decorators";
import pluginTransformRegen from "@babel/plugin-transform-regenerator";
import pluginStyledComponents from "babel-plugin-styled-components";
import pluginCodegen from "babel-plugin-codegen";
import pluginJSXPragmatic from "babel-plugin-jsx-pragmatic";

import {packageHasDep} from "./utils";

export const babelConfig = (command, pkg, options) => {
	const extensions = [...DEFAULT_EXTENSIONS, ".ts", ".tsx", ".json"];
	const { browserslist, format, runtime, pragma, pragmaFrag } = options;

    const jsxPragma = pragma === "React.createElement" ? "React" : pragma;

    let reactPresetOptions = {
        runtime: "classic",
        pragma,
        pragmaFrag,
    };

    if (runtime !== "classic") {
        reactPresetOptions = {
            runtime: "automatic",
            importSource: runtime,
            development: !(process.env.NODE_ENV === "production"),
        };
    }

    const useStyledComponents = packageHasDep(pkg, "styled-components");
    const presets = [
        [
            presetEnv,
            {
                bugfixes: true,
                loose: true,
                useBuiltIns: false,
                modules: false,
                targets: format === "umd" ? "defaults" : browserslist,
                exclude: ["transform-async-to-generator", "transform-regenerator"],
                include: ["transform-block-scoping"],
            },
        ],
        [
            presetTs,
            {
                jsxPragma,
                isTSX: true,
                allExtensions: true,
            },
        ],
        [
            presetReact,
            reactPresetOptions,
        ],
    ];

    const plugins = [
        [pluginDecorators, { legacy: true }],
        [pluginTransformRegen, { async: false }],
        useStyledComponents && pluginStyledComponents,
        pluginCodegen,
        // from mir, get export 'h' and import it as 'h' into whatever files use JSX
        pkg?.roller?.plugins?.pragmatic && [pluginJSXPragmatic, { ...pkg.roller.plugins.pragmatic || { module: "mir", export: "h", import: "h" } }]
    ].filter(Boolean);

    return { presets, plugins, extensions };
};
