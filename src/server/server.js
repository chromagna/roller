const { bright, reset } = require("../utils");
const fs = require("fs");
const url = require("url");
const path = require("path");
const http = require("http");

const mime = Object.entries(require("./types.json")).reduce(
	(all, [type, exts]) => Object.assign(all, ...exts.map((ext) => ({ [ext]: type }))),
	{}
);

const getDefaultopts = (opts) => {
	return {
		root: opts.root ?? ".",
		entry: opts.entry ?? "index.html",
		port: opts.port ?? 8080,
		reloadPort: opts.reloadPort ?? 5000,
        openBrowser: opts.openBrowser ?? true,
		cwd: process.cwd(),
	};
};

const reloadScript = (rp) => `
<script>
    const source = new EventSource("http://localhost:${rp}");
    source.onmessage = e => location.reload(true);
</script>
`;

const sendError = (res, resource, status) => {
	res.writeHead(status);
	res.end();
};

const sendFile = (res, resource, status, file, ext) => {
	res.writeHead(status, {
		"Content-Type": mime[ext] || "application/octet-stream",
		"Access-Control-Allow-Origin": "*",
	});
	res.write(file, "binary");
	res.end();
};

const sendMessage = (res, channel, data) => {
	res.write(`event: ${channel}\nid: 0\ndata: ${data}\n`);
	res.write("\n\n");
};

const isRouteRequest = (uri) => (uri.split("/").pop().indexOf(".") === -1 ? true : false);

export const serve = (opts) => {
	opts = getDefaultopts(opts);

    console.log(` ${bright}${opts.entry}${reset} served from ${bright}http://localhost:${opts.port}${reset}\n`);

	http.createServer((request, res) => {
		res.writeHead(200, {
			Connection: "keep-alive",
			"Content-Type": "text/event-stream",
			"Cache-Control": "no-cache",
			"Access-Control-Allow-Origin": "*",
		});
		sendMessage(res, "connected", "awaiting change");
		setInterval(sendMessage, 60000, res, "ping", "still waiting");
		fs.watch(path.join(opts.cwd, opts.root), {}, () => {
			sendMessage(res, "message", "reloading page");
		});
	}).listen(parseInt(opts.reloadPort, 10));

	http.createServer((request, res) => {
		const pathname = url.parse(request.url).pathname;
		const isRoute = isRouteRequest(pathname);
		const status = isRoute && pathname !== "/" ? 301 : 200;
		const resource = isRoute ? `/${opts.entry}` : decodeURI(pathname);
		const uri = path.join(opts.cwd, opts.root, resource);

		const ext = uri.replace(/^.*[./\\]/, "").toLowerCase();

		fs.stat(uri, (err, _stat) => {
			if (err) return sendError(res, resource, 404);
			fs.readFile(uri, "binary", (err, file) => {
				if (err) return sendError(res, resource, 500);
				if (isRoute) file += reloadScript(opts.reloadPort);
				sendFile(res, resource, status, file, ext);
			});
		});
	}).listen(parseInt(opts.port, 10));

    if (opts.openBrowser) {
        const page = `http://localhost:${opts.port}`;
        const open = process.platform == "darwin" ? "open" : process.platform == "win32" ? "start" : "xdg-open";
        require("child_process").exec(open + " " + page);
    }
};
