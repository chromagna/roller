import cli from "../../package.json";
import { readdirSync, existsSync } from "fs";
import { exec, read, write } from "../utils";
import merge from "deepmerge";
import sort from "sort-package-json";
import { getDefaults, getTemplates } from "./init-create";
import path from "path";
import { red, green, bright, reset } from "../utils";
import mkdir from "mkdirp";
import inquirer from "inquirer";
import {getDefaultPackageOutputs,getDefaultPackageExports, getDefaultPackagePlugins} from "../options";

export const lastPathSegment = (p) => p.split(path.sep).pop();

const gitInfo = () => {
	const cmd = "git config";
	const email = exec(`${cmd} user.email`);
	const user = exec(`${cmd} user.name`);

	return { user, email };
};

const writePackage = async (template, { user, email }, targetPath, opts) => {
	let pkg = {};
	let name = lastPathSegment(targetPath);
	let source = `src/${name}.${template.ext}`;

	if (existsSync(`${targetPath}/package.json`)) {
		pkg = JSON.parse(await read(`${targetPath}/package.json`));
	}

	pkg = merge({ name, version: "0.0.0", license: "MIT", description: "" }, pkg);

    if (user) pkg = merge({ repository: `${user}/${pkg.name}` }, pkg);
    if (email) pkg = merge({ author: `${user} <${email}>` }, pkg);

	pkg = merge(pkg, {
        ...getDefaultPackageOutputs(name),
        type: "module",
        exports: { ...getDefaultPackageExports(name, opts.defaultExport) },
		source,
		files: ["dist"],
        scripts: { build: "roller build", start: "roller start" },
        // devDependencies: { [cli.name]: cli.version },
        roller: { globals: { jquery: "$" }, external: ["moment"], plugins: { ...getDefaultPackagePlugins() } }
	});

    Object.keys(opts).length && Object.keys(opts).forEach(opt => {
        pkg = merge(pkg, { roller: {[opt]: opts[opt]} });
    });

	if (template.name !== "js-cli") {
		pkg = merge(pkg, {
			scripts: { watch: "roller watch" },
            // roller: { entry: "public/index.html", port: "1234" },
		});
	}

	if (template.name !== "js") {
		pkg = merge(pkg, { roller: { example: `public/index.${template.ext}` } });
		if (template.name === "ts" || template.name === "tsx") {
			pkg = merge(pkg, {
				types: `dist/${name}.d.ts`,
				scripts: { postbuild: "tsc -p tsconfig.json" },
				devDependencies: { typescript: cli.devDependencies["typescript"] },
			});
		}
	}

	if (template.name === "js-cli") {
        pkg = merge(pkg, { bin: { [name]: "cli.cjs" } });
	}

	await write(`${targetPath}/package.json`, JSON.stringify(sort(pkg), null, 4));

	return pkg;
};

const writeFiles = async (pkg, template, targetPath) => {
	const files = [...getDefaults(pkg, template), ...getTemplates(pkg, template)];

	for (let { file, content, extensions } of files) {
		let existing = false;

		file = path.join(targetPath, file);

		if (extensions) {
			existing = (await Promise.all(extensions.map((ext) => existsSync(file + ext)))).includes(true);
		} else {
			existing = existsSync(file);
		}

		if (!existing) {
			await write(file, content);
		}
	}
};

export const init = async () => {
    const pragmas = [
        { name: "React.createElement" },
        { name: "h" }
    ];
    const pragmaFrags = [
        { name: "React.Fragment" },
        { name: "Fragment" }
    ];
    const defaultExports = [
        { name: "umd", ext: "umd.js" },
        { name: "mjs", ext: "mjs" },
        { name: "cjs", ext: "cjs" },
    ];
	const templates = [
		{ ext: "js", name: "js" },
		{ ext: "jsx", name: "jsx" },
		{ ext: "js", name: "js-cli" },
		{ ext: "ts", name: "ts" },
		{ ext: "tsx", name: "tsx" },
	];
	let template;
	let name;
    let opts;

	const prompt = [
		{
			type: "input",
			message: "Project name",
			name: "name",
		},
		{
			type: "list",
			name: "templates",
			message: "Please choose a template",
			pageSize: templates.length + 2,
			choices: templates.map((tmpl) => ({
				name: tmpl.name,
				value: tmpl,
			})),
		},
		{
			type: "list",
			name: "pragma",
			message: "Use which JSX pragma?",
            default: "React.createElement",
			pageSize: pragmas.length + 2,
			choices: pragmas.map((prag) => ({ name: prag.name, value: prag.name })),
			when(answers) {
				return ["jsx", "tsx"].includes(answers.templates.name);
			},
		},
		{
			type: "list",
			name: "pragmaFrag",
			message: "Use which JSX Fragment pragma?",
            default: "React.Fragment",
			pageSize: pragmaFrags.length + 2,
			choices: pragmaFrags.map((pf) => ({ name: pf.name, value: pf.name })),
			when(answers) {
				return ["jsx", "tsx"].includes(answers.templates.name);
			},
		},
        {
            type: "list",
            name: "defaultExport",
            message: "Use which bundle for package.json: exports.default?",
            default: "umd",
            pageSize: defaultExports.length + 2,
            choices: defaultExports.map((de) => ({ name: de.name, value: de.ext })),
            when(answers) {
                return ["ts", "js"].includes(answers.templates.name);
            },
        },
	];

	await inquirer.prompt(prompt).then((answers) => {
		template = answers.templates;
		name = answers.name;
        opts = {
            pragma: answers.pragma,
            pragmaFrag: answers.pragmaFrag,
        };
	});

	let targetPath = path.resolve(process.cwd(), name);

	if (!existsSync(targetPath)) mkdir.sync(targetPath);
	else if (readdirSync(targetPath).length !== 0) {
		console.error(` ${red}${path.resolve(targetPath)} is not an empty directory.${reset}`);
		return;
	}

	const pkg = await writePackage(template, gitInfo(), targetPath, opts);
	await writeFiles(pkg, template, targetPath);
};
