import { dtsPlugins, plugins } from "./plugins";
import { existsSync } from "fs-extra";
import { blue, bright, reset } from "./utils";

const defaultInputOptions = {
	inlineDynamicImports: true,
	onwarn: (warning, warn) => warning.code !== "CIRCULAR_DEPENDENCY" && warn(warning),
};

const defaultOutputOptions = {
	esModule: false,
	strict: false,
	freeze: false,
};

export const buildConfig = (command, pkg, options) => {
    const { peerDependencies = {}, devDependencies = {} } = pkg;
    const externals = pkg.roller?.external || [];
	const { name, globals, source: input, main, module, browser, sourcemap, types } = options;

	const external = Object.keys({ ...peerDependencies, ...devDependencies }).concat(...externals);

	const inputOptions = [
		main && {
			...defaultInputOptions,
			external,
			input,
			plugins: plugins(command, pkg, { ...options, format: "cjs" }),
		},
		module && {
			...defaultInputOptions,
			external,
			input,
			plugins: plugins(command, pkg, { ...options, format: "es" }),
		},
		browser && {
			...defaultInputOptions,
			external,
			input,
			plugins: plugins(command, pkg, { ...options, format: "umd" }),
		},
		types && {
			input,
			plugins: dtsPlugins(),
		},
	].filter(Boolean);

	const outputOptions = [
		main && { ...defaultOutputOptions, file: main, format: "cjs", sourcemap },
		module && { ...defaultOutputOptions, file: module, format: "es", sourcemap },
		browser && {
			...defaultOutputOptions,
			file: browser,
			format: "umd",
			name,
			sourcemap,
			globals,
		},
		types && {
			file: types,
			format: "es",
			sourcemap: false,
		},
	].filter(Boolean);

	return { inputOptions, outputOptions };
};

export const startConfig = async (command, pkg, options) => {
	const { name, globals, example, source, module, browser, sourcemap, target } = options;
	const input = existsSync(example) ? example : source;

    if (example !== source && input === example) {
        console.log(` ${bright}example${reset} defined, using ${bright}${example}${reset} for input, not source ${bright}${source}${reset}`);
    }

	let inputOptions;
	let outputOptions;

	if (target === "es") {
		inputOptions = {
			...defaultInputOptions,
			input,
			plugins: plugins(command, pkg, { ...options, format: "es" }),
		};
		outputOptions = {
			...defaultOutputOptions,
			file: module,
			format: "es",
			sourcemap,
		};
	} else if (target === "umd") {
		inputOptions = {
			...defaultInputOptions,
			input,
			plugins: plugins(command, pkg, { ...options, format: "umd" }),
		};
		outputOptions = {
			...defaultOutputOptions,
			file: browser,
			format: "umd",
			name,
			sourcemap,
			globals,
		};
	}

	return { inputOptions, outputOptions };
};
