import { execSync } from "child_process";
import { writeFile, readFile, existsSync } from "fs-extra";
import mkdir from "mkdirp";
import { dirname } from "path";

export const bright = "\x1b[1m";
export const dim = "\x1b[2m";
export const underscore = "\x1b[4m";
export const blink = "\x1b[5m";
export const reverse = "\x1b[7m";
export const hidden = "\x1b[8m";

export const black = "\x1b[30m";
export const red = "\x1b[31m";
export const green = "\x1b[32m";
export const yellow = "\x1b[33m";
export const blue = "\x1b[34m";
export const magenta = "\x1b[35m";
export const cyan = "\x1b[36m";
export const white = "\x1b[37m";

export const BGblack = "\x1b[40m";
export const BGred = "\x1b[41m";
export const BGgreen = "\x1b[42m";
export const BGyellow = "\x1b[43m";
export const BGblue = "\x1b[44m";
export const BGmagenta = "\x1b[45m";
export const BGcyan = "\x1b[46m";
export const BGwhite = "\x1b[47m";

export const reset = "\x1b[0m";

export const camelCased = (str) => str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());

export const packageHasDep = (pkg, dep) => {
	return ["dependencies", "devDependencies", "peerDependencies"].reduce(
		(last, current) => last || (pkg[current] && pkg[current][dep]),
		false
	);
};

export const read = async (p) => await readFile(p, "utf-8");
export const write = async (p, d) => {
	const dest = dirname(p);
	if (!existsSync(dest)) mkdir.sync(dest);
	await writeFile(p, d + "\n", "utf-8");
};

const snakeToCamel = (string) =>
	string.replace(/([-_][a-z])/g, (group) => group.toUpperCase().replace("-", "").replace("_", ""));

export const safePackageName = (string) => snakeToCamel(string.replace("@", "").replace("/", "."));

export const exec = (cmd) =>
	execSync(cmd, { stdio: ["ignore", "pipe", "ignore"] })
		.toString()
		.trim();
