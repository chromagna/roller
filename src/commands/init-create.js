import {camelCased} from "../utils";

const createLicense = (author) => {
	return `MIT License
Copyright (c) ${new Date().getFullYear()} ${author}
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.`;
};

const createCLI = (pkg) => {
	return `#!/usr/bin/env node

const { ${camelCased(pkg.name)} } = require("./${pkg.main}");
const { name, version } = require("./package.json");

const command = process.argv[2];
const usage = () => console.log(\`\${name} \${version}\\nUsage: --\`);

(async () => {
    switch (command) {
        case "help":
            usage();
            break;
        default:
            await ${camelCased(pkg.name)}(command);
            break;
    }
})();`;
};

const createCLISource = (pkg) => {
	return `export const ${camelCased(pkg.name)} = async (command) => {
    switch (command) {
        default:
            break;
    }
}`;
};

const createIndex = (pkg) => {
	return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>${pkg.name}</title>
  </head>
  <body>
    <div id="root"></div>
    <script src="${pkg.module}" type="module"></script>
  </body>
</html>`;
};

export const createTsConfig = () => `{
  "include": ["src"],
  "compilerOptions": {
    "target": "esnext",
    "module": "esnext",
    "moduleResolution": "node",
    "declaration": true,
    "declarationDir": "dist/types",
    "emitDeclarationOnly": true,
    "jsx": "react",
    "lib": ["dom", "esnext"],
    "removeComments": true,
    "allowSyntheticDefaultImports": true
  }
}`;

const createReadme = (name) => `# ${name}
`;

export const getDefaults = (pkg, template) =>
	[
		{
			file: "README.md",
			content: createReadme(pkg.name),
			extensions: [".md", ".txt"],
		},
		pkg.author && {
			file: "LICENSE",
			content: createLicense(pkg.author),
			extensions: ["", ".md", ".txt"],
		},
		{
			file: ".gitignore",
			content: ["node_modules", "coverage", ".idea", "*.log"].join("\n"),
		},
        {
            file: ".prettierrc",
            content: `{\n\t"printWidth": 120,\n\t"singleQuote": false,\n\t"semi": true,\n\t"useTabs": true,\n\t"tabWidth": 4\n}`,
        },
		template.name !== "js-cli" && {
			file: "public/index.html",
			content: createIndex(pkg),
		},
		template.name !== "js-cli" && {
			file: `public/index.${template.ext}`,
			content: `import { sum } from "../src/${pkg.name}";\n\nconsole.log("this works => ", sum(2, 3));`,
		},
	].filter(Boolean);

const getJavascriptDefaults = (pkg, template) => {
	return [
		{
			file: pkg.source,
			content: `export const sum = (a, b) => a + b;`,
		},
	];
};

const getTypescriptDefaults = (pkg, template) => {
	return [
		{
			file: pkg.source,
			content: `export const sum = (a: number, b: number): number => a + b;`,
		},
		{
			file: "tsconfig.json",
			content: createTsConfig(),
		},
	];
};

export const getTemplates = (pkg, template) => {
	const templates = {
		js: [...getJavascriptDefaults(pkg, template)],
		jsx: [...getJavascriptDefaults(pkg, template)],
		"js-cli": [
			{
				file: "link.sh",
				content: `#!/usr/bin/env bash\n\nwhich ${pkg.name} > /dev/null 2>&1 && npm unlink -g || npm link`,
			},
			{
				file: "cli.cjs",
				content: createCLI(pkg),
			},
			{
				file: pkg.source,
				content: createCLISource(pkg),
			},
		],
		ts: [...getTypescriptDefaults(pkg, template)],
		tsx: [...getTypescriptDefaults(pkg, template)],
	};

	return templates[template.name];
};
