import { rollup, watch } from "rollup";
import { init } from "./commands/init";
import { read, bright, reset } from "./utils";
import { getOptions, changePackage, revertPackage, changeTsconfig, revertTsconfig } from "./options";
import { buildConfig, startConfig } from "./configs";
import path from "path";

const writeBundle = async (bundle, outputOptions) => {
	await bundle.generate(outputOptions);
	await bundle.write(outputOptions);
};

const build = async (options, inputOptions) => {
	try {
		const bundle = await rollup(inputOptions);
		await writeBundle(bundle, options);
	} catch (error) {
        console.error(error);
	}
};

const ROLLUP_LISTENERS = {
	ERROR: (event) => console.error(event.error.message),
	BUNDLE_START: () => {},
	BUNDLE_END: (event) => {
		const input = event.input.toString();
		const output = event.output.toString();

		console.info(
			` ${bright}${input}${reset} -> ${bright}${output.replace(process.cwd()+path.sep, "")}${reset} ${event.duration / 1000.0}s`
		);
	},
};

const showdbg = (command, options, config) => {
    let inp, x;
    if (command === "watch" || command === "build") {
        x = 0;
        for (inp of config.inputOptions) {
            x !== 0 && console.log();
            console.log(` <- ${bright}${inp["input"]}${reset} `);
            for (let p of inp["plugins"]) console.log("   ", p.name);
            console.log(` -> ${bright}${Object.entries(config.outputOptions)[x][1].file}${reset}`);
            x++;
        }
    }

    if (command === "start") {
        console.log(`\n <- ${bright}${config.inputOptions["input"]}${reset} `);
        for (let p of config.inputOptions["plugins"]) console.log("   ", p.name);
        console.log(` -> ${bright}${config.outputOptions.file}${reset} (format: ${config.outputOptions.format})`);
    }
};

const roller = async (command, pkg) => {
	let options = getOptions(pkg, command);
	let config, watchOptions;
    let p, t;

    switch (command) {
        case "build":
            config = buildConfig(command, pkg, options);
            break;
        case "watch":
            config = buildConfig(command, pkg, options);
            break;
        case "start":
            config = await startConfig(command, pkg, options)
    }

    options.debug && showdbg(command, options, config);

	switch (command) {
        case "convert":
            p = await changePackage(pkg);
            t = await changeTsconfig();

            !p && !t && console.log(" nothing to convert");

            p && console.log(`${bright} package.json converted${reset}`);
            t && console.log(`${bright} tsconfig.json converted${reset}`);

            break;
        case "revert":
            p = await revertPackage();
            t = await revertTsconfig();

            !p && !t && console.log(" nothing to revert");

            p && console.log(`${bright} package.json reverted${reset}`);
            t && console.log(`${bright} tsconfig.json reverted${reset}`);
            break;
		case "build":
            config.outputOptions.map((output, index) =>  build(output, config.inputOptions[index]));
			break;
		case "watch":
            watchOptions = config.outputOptions.map((output, index) => ({ ...config.inputOptions[index], output }));
            watch(watchOptions).on("event", event => ROLLUP_LISTENERS[event.code] && ROLLUP_LISTENERS[event.code](event));
			break;
		case "start":
            console.log(`\n <- ${bright}${config.inputOptions.input}${reset}\n -> ${bright}${config.outputOptions.file}${reset}\n`);
            watchOptions = { ...config.inputOptions, output: config.outputOptions };
            watch(watchOptions).on("event", event => ROLLUP_LISTENERS[event.code] && ROLLUP_LISTENERS[event.code](event));
			break;
	}
};

export { init, roller, read, reset };
export { red, green, bright } from "./utils";
