# roller

**Roll**up wrapp**er**. Roller.

Roller was originally created as a companion build tool for https://gitlab.com/chromagna/mir. Roller with mir is a
very enjoyable frontend development experience. Give it a try!

![roller build](./.readme/roller_build.gif)
![roller start](./.readme/roller_start.gif)

[toc]

# Quickstart

```bash
# Install it
npm install --global https://gitlab.com:chromagna/roller

# Start your next project
roller init
```

# Core configuration

All roller configuration should exist in your package.json. This is how roller decides
which bundles to generate.

```javascript
{
    "name": "my-project", // used as the bundle name
    "source": "src/index.js", // path to source input (--source <path>)
    "main": "dist/bundle.cjs", // path to cjs output bundle (--cjs <path>)
    "module": "dist/bundle.mjs", // path to esm output bundle (--esm <path>)
    "browser": "dist/bundle.umd.js", // path to umd output bundle (--umd <path>)
    "types": "dist/bundle.d.ts", // path to output types (--types <path>)
    "roller": {
        "entry": "public/index.html", // html entrypoint for `roller start`
        "example": "public/index.js", // source entrypoint for `roller start` (--source will overwrite this)
        "port": "1234", // port used by `roller start`
        "openBrowser": false, // Should `roller start` automatically open your browser?
        "runtime": "automatic", // React runtime [classic, automatic]: https://reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html
        "pragma": "h", // For custom JSX pragma. Consumed by @rollup/plugin-babel. See roller.plugins.pragmatic for additional config
        "pragmaFrag": "Fragment", // For custom JSX Fragment pragma
        "debug": false,
        "minify": true // minify output bundle on build?
    }
}
```

Explicitly defined CLI flags will take precedence over any package.json configs.

```bash
roller build --source src/main.js --esm dist/bundle.mjs --cjs dist/bundle.cjs --umd dist/bundle.umd.js
```

# Plugin configuration

You can further configure certain plugins.

```javascript
{
    "roller": {
        "plugins": {
            // Below is a list of available terser configurations.
            "terser": {
                "toplevel": true,
                "keep_fnames": true,
                "mangle": {
                    "properties": {
                        "regex": "^[\\$\\$]"
                    }
                },
                "compress": {
                    "passes": 10,
                    "pure_getters": true
                },
                "filter": {
                    "include": [],
                    "exclude": [],
                    "resolve": false
                }
            },
            // babel-plugin-jsx-pragmatic
            // Using a custom pragma? Avoid having to `import { h } from "mir";` into every file that has JSX.
            "pragmatic": {
                "module": "mir", // From the "mir" module...
                "export": "h", // ...using the named export "h"...
                "import": "h" // ...import it as "h" into every file that uses JSX.
            }
        }
    }
}
```

# Bundled dependencies

Package dependencies will be included in your bundle. devDependencies and peerDependencies will not.

# Output

## name

https://rollupjs.org/guide/en/#outputname


Your package.json "name" property is used as output.name.

## Formats

- If your package.json has a "module" property, then a rollup bundle output is defined with format "es".
- If your package.json has a "main" property, then a rollup bundle output is defined with format "cjs".
- If your package.json has a "browser" property, then a rollup bundle output is defined with format "umd".

## Globals

https://rollupjs.org/guide/en/#outputglobals

```javascript
{
    roller: {
        globals: {
            jquery: "$"
        }
    }
}
```

## Externals

https://rollupjs.org/guide/en/#warning-treating-module-as-external-dependency

As the rollup documentation implies, the purpose of this configuration option is to simply suppress
the warning that rollup generates when using non-relative module imports, like:

```javascript
import moment from "moment";
```

...which normally would generate this warning:

> Warning: "Treating moment as external dependency"

```javascript
{
    roller: {
        external: ["moment"]
    }
}
```

