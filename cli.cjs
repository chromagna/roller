#!/usr/bin/env node

const {existsSync} = require("fs");
const path = require("path");
const { roller, init, read, bright, reset } = require("./dist");
const { name, version } = require("./package.json");
const command = process.argv[2];

const defaultEnv = {
	build: "production",
	prod: "production",
	watch: "development",
	start: "development",
};

const usage = () => {
    console.log(` ${bright}${version}${reset}

 ${bright}build${reset}     bundle your project in production mode
 ${bright}watch${reset}     bundle your project and watch for changes
 ${bright}start${reset}     start a development server
 ${bright}convert${reset}   creates backups of package.json & tsconfig.json before
           writing new ones for easily testing roller against a project
 ${bright}revert${reset}    revert operations done by convert`);
};

(async () => {
    let pkg;
	switch (command) {
		case "init":
			await init(command);
			break;
        case "convert":
        case "revert":
		case "build":
		case "start":
		case "watch":
            if (!existsSync(path.join(process.cwd(), "package.json"))) {
                console.log("no package.json found - please make sure to run this from a directory where package.json exists.");
                return;
            }
			process.env.NODE_ENV = process.env.NODE_ENV || defaultEnv[command];
			pkg = JSON.parse(await read(path.join(process.cwd(), "package.json")));
			await roller(command, pkg);
			break;
		case "help":
            usage();
			break;
		default:
            usage();
			break;
	}
})();
